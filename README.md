## 项目介绍

是基于 Vue3 + Vite5+ TypeScript5 + Element-Plus + Pinia 等最新主流技术栈构建的后台管理系统

## 项目预览

- **在线预览**： []()


## 项目地址
https://repo1.antiy.cn/cdbc/bc-web-view/ctf.git

## 环境准备

| 环境                 | 名称版本                                                     | 
| -------------------- | :----------------------------------------------------------- | 
| **开发工具**         | VSCode                                                       | 
| **运行环境**         | Node 18+                                                     | 
| **VSCode插件(必装)** | 1. `Vue Language Features (Volar) ` <br/> 2. `TypeScript Vue Plugin (Volar) `  <br/>3. 禁用 Vetur


## 项目启动

```bash
# 克隆代码
git clone https://repo1.antiy.cn/cdbc/bc-web-view/ctf.git

# 切换目录
cd ctf

# 安装 pnpm
npm install pnpm -g

# 安装依赖
pnpm install

# 启动运行
pnpm run dev
```


## 项目部署

```bash
# 项目打包
pnpm run build:prod

# 上传文件至远程服务器
将打包生成在 `dist` 目录下的文件拷贝至 `/usr/local/sod-deploy/infrastructure/nginx/html` 目录
```

## 注意事项

- **自动导入插件自动生成默认关闭**

  模板项目的组件类型声明已自动生成。如果添加和使用新的组件，请按照图示方法开启自动生成。在自动生成完成后，记得将其设置为 `false`，避免重复执行引发冲突。

  ![](https://foruda.gitee.com/images/1687755823137387608/412ea803_716974.png)

- **项目启动浏览器访问空白**

  请升级浏览器尝试，低版本浏览器内核可能不支持某些新的 JavaScript 语法，比如可选链操作符 `?.`。

- **项目组件、函数和引用爆红**

	重启 VSCode 尝试

## 后端接口

- **后端接口地址**：[]()

- **接口文档地址**：[在线接口文档]()



## 提交规范

Vscode安装插件git-commit-plugin，使用该插件根据提示完成信息的输入和选择


