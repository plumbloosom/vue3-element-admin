import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { useUserStoreHook } from "@/store/modules/user";
import { loginJump } from "@/utils/common";
NProgress.configure({ showSpinner: false }); // 进度条
// 白名单路由
const whiteList = ["/login", "/401", "/404"];

export const Layout = () => import("@/layout/index.vue");

// 静态路由
export const constantRoutes: RouteRecordRaw[] = [
  {
    path: "/redirect",
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: "/redirect/:path(.*)",
        component: () => import("@/views/redirect/index.vue"),
      },
    ],
  },
  {
    path: "/login",
    component: () => import("@/views/login/index.vue"),
    meta: { hidden: true },
  },
  {
    path: "/layout",
    component: Layout,
    children: [
      {
        path: "/dashboard",
        component: () => import("@/views/dashboard/index.vue"),
        name: "Dashboard", // 用于 keep-alive, 必须与SFC自动推导或者显示声明的组件name一致
        // https://cn.vuejs.org/guide/built-ins/keep-alive.html#include-exclude
        meta: {
          title: "dashboard",
          icon: "homepage",
          affix: true,
          keepAlive: true,
          alwaysShow: false,
          level: 1,
          permission: "tp",
        },
      },
    ],
  },
  {
    path: "/401",
    component: () => import("@/views/error-page/401.vue"),
    meta: { hidden: true },
  },
  {
    path: "/404",
    component: () => import("@/views/error-page/404.vue"),
    meta: { hidden: true },
  },
];

/**
 * 创建路由
 */
const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes as RouteRecordRaw[],
  // 刷新时，滚动条位置还原
  scrollBehavior: () => ({ left: 0, top: 0 }),
});
/* 路由拦截处理 */
router.beforeEach(async (to, from, next) => {
  NProgress.start();
  const userStore = useUserStoreHook();
  // 本地缓存中是否有用户信息(系统本身有可能已经登录)
  let userInfo = window.sessionStorage.getItem("userInfo");
  let storageToken = userStore.token;
  let storagePermission = userStore?.permission;
  if (userInfo) {
    userInfo = JSON.parse(userInfo);
    storageToken = userStore?.token || userInfo?.token;
    storagePermission = userStore?.permission || userInfo?.permission;
  }
  let redirectUrl = "";
  if (process.env.NODE_ENV === "development" && !storageToken) {
    redirectUrl = "/login";
  }
  // 异常处理 401 404
  // if (to.matched.length === 0 && !from.name) {
  //   next("/404");
  // }
  // 获取有权限的路由
  const layoutRoute = constantRoutes.find((v) => v.path === "/layout");
  // 获取一级菜单
  const leve1Menu = layoutRoute.children.filter((v) => v.meta?.level === 1);
  const hasAuthPath =
    leve1Menu.filter((v) => storagePermission.includes(v.meta.permission)) ||
    [];
  if (hasAuthPath.length) {
    redirectUrl = hasAuthPath[0].path;
  }
  if (storageToken) {
    if (to.meta.permission && !storagePermission.includes(to.meta.permission)) {
      // 无权限时，跳转到401页面
      if (to.fullPath === "/login" && process.env.NODE_ENV === "development") {
        next({ path: "/login" });
      } else {
        next({ path: "/401", replace: true });
      }
    } else {
      if (
        to.fullPath === "/" ||
        (process.env.NODE_ENV === "development" &&
          to.fullPath === "/layout" &&
          redirectUrl)
      ) {
        next({ path: redirectUrl });
      } else {
        next();
      }
    }
  } else {
    if (to.path !== "/login") {
      // 跳转ua登录
      if (process.env.NODE_ENV === "development") {
        next("/login");
      } else {
        loginJump();
      }
    } else {
      next();
    }
    NProgress.done();
  }
});
router.afterEach(() => {
  NProgress.done();
});
/**
 * 重置路由
 */
export function resetRouter() {
  router.replace({ path: "/login" });
}

export default router;
