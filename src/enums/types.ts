/**
 * 接口响应参数
 */
interface headObj {
  code: string;
  result: string;
}
export interface responseData {
  head: any;
  // {"code": "200","result": "成功"}
  body: headObj;
}
