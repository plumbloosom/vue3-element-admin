import request from "@/utils/request";
import { AxiosPromise } from "axios";
import { LoginData, LoginResult } from "./types";
export default {
  login: (params: LoginData) => request("/oauth/sys/login", params, "post"), // 登录
  logout: (params: any) => request("/oauth/sys/remove_token", params, "post"), // 登出
  changePwd: (params: any) => request("/user/update/password", params, "post"), // 修改密码
};
