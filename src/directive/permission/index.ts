import { useUserStoreHook } from "@/store/modules/user";
import { Directive, DirectiveBinding } from "vue";

/**
 * 按钮权限
 */
function checkPermission(el: HTMLElement, binding: DirectiveBinding) {
  const { permission } = useUserStoreHook().user;
  // 「角色」按钮权限校验
  const { value } = binding;
  if (value) {
    const requiredPerms = value; // DOM绑定需要的按钮权限标识

    const hasPerm = permission?.some((perm) => {
      return requiredPerms.includes(perm);
    });

    if (!hasPerm) {
      el.parentNode && el.parentNode.removeChild(el);
    }
  }
}
export const permission: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    checkPermission(el, binding);
  },
};
