import CryptoJS from "crypto-js";
/**
 * 登录密码加密
 * @param  {string} word 待加密的数据
 * @return {string}      加密后的数据
 */
const encryptForLogin = (word: string) => {
  const key = CryptoJS.enc.Utf8.parse("abcdefgabcdefg12");
  const encrypted = CryptoJS.HmacSHA256(word, key);
  return encrypted.toString();
};
// 登录失效跳转到ua登录
const loginJump = () => {
  if (process.env.NODE_ENV === "development") {
    ElMessage.warning("开发环境不支持跳转ua登录，跳转本地登录");
    window.location.href = `${window.location.origin}/#/login`;
  } else {
    window.location.href = window.location.origin + "/ua/#/";
  }
  // 清除缓存数据
  window.sessionStorage.removeItem("userInfo");
};
export { encryptForLogin, loginJump };
