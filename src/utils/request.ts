import axios, { InternalAxiosRequestConfig, AxiosResponse } from "axios";
import { useUserStoreHook } from "@/store/modules/user";
import { loginJump } from "@/utils/common";
import { ElMessage, ElLoading } from "element-plus";
import qs from "qs";
import { debounce } from "lodash-es";
const NO_MSG_URL: string[] = [];
const NO_AUTHORIZATION_URL: string[] = [
  "/api/v1/oauth/sys/login", // 登录
];
function checkStatus(res: any) {
  if (res.status >= 200 && res.status < 300) {
    if (res.data?.head?.code === "200") {
      return res;
    } else if (["421"].includes(res.data?.head?.code)) {
      // 登录失效处理
      loginJump();
    }
  }
  throw res;
}
function toast(err: any) {
  let msg;
  if (err.status === 404 || err.response?.status === 404) {
    msg = "找不到该请求";
  } else if (err.data) {
    msg = err.data.msg || err.data.body || err.data.head?.result;
  } else {
    msg = "请求失败";
  }
  ElMessage.closeAll();
  if (!NO_MSG_URL.includes(err.config.url)) ElMessage.warning(msg);
}
const fetch = (
  url: string,
  data: any = {},
  method = "get",
  headers: any,
  longUrl: string
) => {
  const userStore = useUserStoreHook();
  if (!longUrl) {
    url = `/api/v1${url}`;
    const Authorization = NO_AUTHORIZATION_URL.includes(url)
      ? {}
      : { Authorization: `Bearer ${userStore.token}` };
    headers = { ...Authorization, ...headers };
  } else {
    url = longUrl;
  }
  if (method === "get") {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: `${url}${qs.stringify(data) ? "?" + qs.stringify(data) : ""}`,
        headers,
      })
        .then(checkStatus)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          toast(err);
          reject(err);
        });
    });
  }
  if (method === "post" || method === "put") {
    if (!(data instanceof FormData)) {
      headers["Content-Type"] = "application/json; charset=UTF-8";
      data = JSON.stringify(data);
    }

    return new Promise((resolve, reject) => {
      axios({
        method,
        url,
        data,
        headers,
      })
        .then(checkStatus)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          // fix:文件上传，已上传的文件修改文件名，异常处理
          if (!(data instanceof FormData)) {
            toast(err);
          } else {
            if (err?.status) {
              // 接口返回异常
              toast(err);
            } else {
              // 修改文件名导致xhr异常
              ElMessage.error("文件异常，请重新上传！");
            }
          }
          reject(err);
        });
    });
  }
  if (method === "delete") {
    return new Promise((resolve, reject) => {
      axios({
        method: "delete",
        url: `${url}/${data}`,
        headers,
      })
        .then(checkStatus)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          toast(err);
          reject(err);
        });
    });
  }
};
// 增加封装Axios实现全局的loading自动显示效果（结合Element UI），可指定元素显示loading
// loading对象
let loading: any;

// 当前正在请求的数量
let needLoadingRequestCount = 0;

// 显示loading
function showLoading(target: string) {
  // 后面这个判断很重要，因为关闭时加了抖动，此时loading对象可能还存在，
  // 但needLoadingRequestCount已经变成0.避免这种情况下会重新创建个loading
  if (needLoadingRequestCount === 0 && !loading) {
    loading = ElLoading.service({
      lock: true,
      text: "加载中...",
      background: "rgba(0, 0, 0, 0.3)",
      target: target || "body",
    });
  }
  needLoadingRequestCount++;
}
// 隐藏loading
function hideLoading() {
  needLoadingRequestCount--;
  needLoadingRequestCount = Math.max(needLoadingRequestCount, 0); // 做个保护
  if (needLoadingRequestCount === 0) {
    // 关闭loading
    toHideLoading();
  }
}
// 防抖：将 300ms 间隔内的关闭 loading 便合并为一次。防止连续请求时， loading闪烁的问题。
// eslint-disable-next-line no-var
var toHideLoading = debounce(() => {
  loading && loading.close();
  loading = null;
}, 300);
// 添加请求拦截器
axios.interceptors.request.use(
  (config) => {
    // 判断当前请求是否设置了不显示Loading
    if (config.headers?.showLoading) {
      // 请求显示loading 效果
      showLoading(config.headers.loadingTarget);
    }
    return config;
  },
  (error) => {
    // 对请求错误做些什么
    // 判断当前请求是否设置了不显示Loading
    if (error.headers?.showLoading) {
      hideLoading();
    }
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  (response) => {
    // 判断当前请求是否设置了不显示Loading（不显示自然无需隐藏）
    if (response.config.headers?.showLoading) {
      hideLoading();
    }
    return response;
  },
  (error) => {
    // 判断当前请求是否设置了不显示Loading（不显示自然无需隐藏）
    if (error.config.headers?.showLoading) {
      hideLoading();
    }
    return Promise.reject(error);
  }
);

export default fetch;
