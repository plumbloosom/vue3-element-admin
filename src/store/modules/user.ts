import { defineStore } from "pinia";
import api from "@/api";
import { store } from "@/store";
interface LoginData {
  /**
   * 用户名
   */
  username: string;
  /**
   * 密码
   */
  password: string;
}

interface UserInfo {
  token?: string;
  userid?: number;
  name?: string;
  username?: string;
  permission: string[];
}
export const useUserStore = defineStore("userInfo", {
  state: () => ({
    token: "",
    userid: "", // 修改密码和连接ws用的
    name: "", // header上显示用的
    username: "", // 接口使用
    permission: [],
  }),
  actions: {
    login(params: LoginData) {
      return new Promise<void>((resolve, reject) => {
        api
          .login(params)
          .then((res) => {
            this.token = res.body.access_token;
            this.userid = res.body.user_info.id;
            this.name = res.body.access_token;
            this.username = res.body.user_info.name;
            this.permission = res.body.user_info.permissions;
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
  },
  //配置缓存
  persist: {
    key: "userInfo",
    //存储在localstorage还是sessionStorage中
    storage: sessionStorage,
    //哪个数据需要持久化
    paths: ["token", "userid", "name", "username", "permission"],
  },
});
// 非setup
export function useUserStoreHook() {
  return useUserStore(store);
}
